#!/usr/bin/env python3.5
import mutagen.flac, os, re, sys

filename = sys.argv[1]
acceptable_sample_rates = re.compile(r'(48000)|(44100)')

if sys.argv[1][-5:]=='.flac':
    # if the filename ends in .flac
    fi = mutagen.flac.Open(filename)
    # open the file with mutagen
    if not re.fullmatch(acceptable_sample_rates, fi.info['sample_rate']):
        # if the sample rate isn't 48k or 44.1k jump to the ffmpeg process
        os.execlp("ffmpeg -i {} -sample_fmt s16 -ar 48000 -f flac -".format(filename))
